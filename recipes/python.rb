#
# Cookbook:: aon3d
# Recipe:: default
#
# Copyright:: 2021, AON3D, All Rights Reserved.

# Install Python3.6:
#apt_repository 'deadsnakes' do
#  uri 'ppa:deadsnakes/ppa'
#  action :add
#end
#
#apt_update
#
#package 'python3.6'

# pip install importlib-metadata

pkgs = [
  "build-essential",
  "libssl-dev",
  "zlib1g-dev",
  "libncurses5-dev",
  "libncursesw5-dev",
  "libreadline-dev",
  "libsqlite3-dev",
  "libgdbm-dev",
  "libdb5.3-dev",
  "libbz2-dev",
  "libexpat1-dev",
  "liblzma-dev",
  "tk-dev",
  "libffi-dev",
]

pkgs.each do |pkg|
  package pkg
end

# Installing pipenv:
# sudo curl https://raw.githubusercontent.com/pypa/pipenv/master/get-pipenv.py | python
