#
# Cookbook:: aon3d
# Recipe:: default
#
# Copyright:: 2021, AON3D, All Rights Reserved.

%w(
  arduino-core
  avr-libc
  avrdude
  doxygen
  gcc-avr
  graphviz
  latexmk
  libdbus-glib-1-dev
  make
  python3-pip
  texlive-full
  texlive-binaries
  texlive-latex-extra
  texlive-latex-recommended
)

include_recipe "aon3d::python"
include_recipe "aon3d::bashrc"

directory '/tmp/mobius'
