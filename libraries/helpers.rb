class Chef
  class Recipe
    def aon3drc(bashrc_path)
      line = 'if [ -f "$HOME/.aon3drc" ]; then source "$HOME/.aon3drc"; fi'

      file = Chef::Util::FileEdit.new(bashrc_path)
      file.insert_line_if_no_match(/#{line}/, line)
      file.write_file
    end
  end
end
